import Router from 'vue-router'

import Home from './Home'
const Battle = () => import(/* webpackChunkName: "battle", webpackPrefetch: true */ './Battle')
const Results = () => import(/* webpackChunkName: "results", webpackPrefetch: true */ './Results')
const Popular = () => import(/* webpackChunkName: "popular", webpackPrefetch: true */ './Popular')
import NotFound from './NotFound'

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/battle', name: 'battle', component: Battle },
		{ path: '/battle/results', name: 'results', component: Results },
    { path: '/popular', name: 'popular', component: Popular },
    { path: '*', name: '404', component: NotFound },
  ],
})
