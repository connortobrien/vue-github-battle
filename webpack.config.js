const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const env = require('./config.json')

module.exports = {
  entry: path.join(__dirname, 'src/index.js'),
  mode: process.env.NODE_ENV || 'development',
  output: {
    path: path.join(__dirname, '/dist'),
    filename: '[name].bundle.js',
		chunkFilename: '[name].bundle.js',
		publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader',
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 },
          },
          {
            loader: 'postcss-loader',
            options: {
              parser: 'sugarss',
              plugins: [
                require('autoprefixer'),
                require('postcss-nested'),
              ],
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.vue'],
  },
  plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('development'),
				CLIENT_ID: JSON.stringify(env.CLIENT_ID),
				CLIENT_SECRET: JSON.stringify(env.CLIENT_SECRET),
			},
		}),
    new webpack.HotModuleReplacementPlugin(),
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'public/index.html'),
    }),
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    hot: true,
    historyApiFallback: true,
  },
};
