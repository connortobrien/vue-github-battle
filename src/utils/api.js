const { CLIENT_ID, CLIENT_SECRET } = process.env

const authParams = `?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}`

const getProfile = async (username) => (await fetch(`https://api.github.com/users/${username}${authParams}`)).json()

const getRepos = async (username) => (await fetch(`https://api.github.com/users/${username}/repos${authParams}&per_page=100`)).json()

const getStarCount = (repos) => repos.reduce((count, repo) => count + repo.stargazers_count, 0)

const calculateScore = (profile, repos) => {
	const { followers } = profile
	const totalStars = getStarCount(repos)
	return (followers * 3) + totalStars
}

const getUserData = async (player) => {
	const [profile, repos] = await Promise.all([getProfile(player), getRepos(player)])
	return { profile, score: calculateScore(profile, repos) }
}

const sortPlayers = (players) => players.sort((a, b) => b.score - a.score)

export const battle = async (playerOne, playerTwo) => {
	return sortPlayers(await Promise.all(
		[playerOne, playerTwo].map(getUserData),
	))
}

export const fetchPopularRepos = async (language) => {
	const encodedURI = window.encodeURI(`https://api.github.com/search/repositories?q=stars:>1+language:${language}&sort=stars&order=desc&type=Repositories${authParams.replace('?', '&')}`)
	return (await (await fetch(encodedURI)).json()).items
}
