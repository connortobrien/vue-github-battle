import { shallowMount } from '@vue/test-utils'

import PlayerAvater from './PlayerAvatar'

describe('PlayerAvater', () => {
	let wrapper

	beforeEach(() => {
		wrapper = shallowMount(PlayerAvater)
	})

	it('renders', () => {
		expect(wrapper.element).toMatchSnapshot()
	})
})
