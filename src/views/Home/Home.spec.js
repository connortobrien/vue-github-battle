import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import Home from '.'

const localVue = createLocalVue()
localVue.use(VueRouter)
const routes = [
	{ name: 'battle', path: '/battle' },
]
const router = new VueRouter({ routes })

describe('Home', () => {
	it('renders', () => {
		const wrapper = shallowMount(Home, { localVue, router })
		expect(wrapper.element).toMatchSnapshot()
	})

	it('renders the welcome message', () => {
		const wrapper = shallowMount(Home, { stubs: ['router-link'] })
		expect(wrapper.find('h1').exists()).toBe(true)
		expect(wrapper.find('h1').text()).toBe('Github Battle: Battle your friends... and stuff.')
	})

	it('renders a button', () => {
		const wrapper = mount(Home, { localVue, router })
		expect(wrapper.find('button').exists()).toBe(true)
		expect(wrapper.find('button').text()).toBe('Battle')
	})
})
