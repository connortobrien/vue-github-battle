import { shallowMount } from '@vue/test-utils'

import App from './App'

describe('App', () => {
	let wrapper

	beforeEach(() => {
		wrapper = shallowMount(App, { stubs: ['router-view'] })
	})
	
	it('renders', () => {
		expect(wrapper.element).toMatchSnapshot()
	})

	it('creates a div #id wrapper', () => {
		expect(wrapper.find('div#app').exists()).toBe(true)
	})

	it('creates a content class wrapper', () => {
		expect(wrapper.find('div.content').exists()).toBe(true)
	})
})
