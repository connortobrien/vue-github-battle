import Vue from 'vue'
import Router from 'vue-router'
import 'core-js/stable'
import 'regenerator-runtime/runtime'

import App from './components/App'
import router from './views/router'
import './global.css';

Vue.use(Router)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
