import { shallowMount } from '@vue/test-utils'

import Loading from './Loading'

jest.useFakeTimers()

describe('Loading', () => {
	let wrapper

	beforeEach(() => {
		wrapper = shallowMount(Loading)
	})

	it('renders', () => {
		expect(wrapper.element).toMatchSnapshot()
	})

	it('displays the default loading text', () => {
		expect(wrapper.text()).toBe('Loading')
	})

	it('displays the passed in value if provided', () => {
		const text = 'Hello World'
		wrapper.setProps({ text })
		expect(wrapper.props().text).toBe(text)
	})

	it('sets the default speed', () => {
		expect(wrapper.props().speed).toBe(300)
	})

	it('sets the speed from the prop passed in', () => {
		const speed = 500
		wrapper.setProps({ speed })
		expect(wrapper.props().speed).toBe(500)
	})

	it('has a computed value for stop text', () => {
		const text = 'Loading'
		const localThis = { text }
		expect(Loading.computed.stopper.call(localThis)).toBe(`${text}...`)
	})

	it('has a created hook', () => {
		expect(typeof Loading.created).toBe('function')
	})

	it('sets interval on create', () => {
		Loading.created()
		expect(setInterval).toHaveBeenCalled();
	})

	it('calls setInterval with a method \'updateLoadingText\' and speed', () => {
		const speed = 300
		wrapper.setProps({ speed })
		expect(setInterval).toHaveBeenCalledWith(wrapper.vm.updateLoadingText, speed)
	})

	it('has a beforeDestroy hook', () => {
		expect(typeof Loading.beforeDestroy).toBe('function')
	})

	it('removes the interval before destroy', () => {
		Loading.beforeDestroy()
		expect(clearInterval).toHaveBeenCalledTimes(1);
	})

	it('removes the interval funciton \'updateLoadingText\' on destroy', () => {
		wrapper.destroy()
		expect(clearInterval).toHaveBeenCalledWith(wrapper.vm.updateLoadingText)
	})

	it('creates a method for updating loading text', () => {
		expect(typeof wrapper.vm.updateLoadingText).toBe('function')
	})

	it('should add a period if !== stop value', () => {
		wrapper.vm.updateLoadingText()
		expect(wrapper.vm.loadingText).toBe('Loading.')
	})

	it('should set value back to text if === to stop value', () => {
		const text = 'Loading'
		wrapper.setData({ loadingText: `${text}...` })
		wrapper.vm.updateLoadingText()
		expect(wrapper.vm.loadingText).toBe(text)
	})
})
