import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import NavBar from './NavBar'

const localVue = createLocalVue()
localVue.use(VueRouter)
const routes = [
	{ name: 'home', path: '/home' },
	{ name: 'battle', path: '/battle' },
	{ name: 'popular', path: '/popular' },
]
const router = new VueRouter({ routes })

describe('NavBar', () => {
	let wrapper

	beforeEach(() => {
		wrapper = shallowMount(NavBar, { localVue, router })
	})

	it('renders', () => {
		expect(wrapper.element).toMatchSnapshot()
	})
})
